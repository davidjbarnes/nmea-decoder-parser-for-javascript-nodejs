# NMEA Decoder/Parser for Javascript/NodeJS #

![2015-07-02 08.38.39.jpg](https://bitbucket.org/repo/GEkMkk/images/1160577899-2015-07-02%2008.38.39.jpg)

Example usage: $ node index.js -f nmeaGps.txt

Example NMEA GPS Text File (nmeaGps.txt):
$GPGLL,3927.2670,N,10711.1482,W,224125.200,A,A*4E
$GPGSA,A,3,30,17,28,13,11,19,07,,,,,,1.38,1.07,0.87*03
$GPGSV,3,1,10,30,81,019,19,28,59,290,28,07,57,111,28,51,44,180,28*7E
$GPGSV,3,2,10,11,37,102,27,13,31,309,21,19,25,044,29,17,25,203,27*73
$GPGSV,3,3,10,04,22,090,,09,11,174,*7E
$GPRMC,224125.200,A,3927.2670,N,10711.1482,W,0.08,122.75,240415,,,A*74
$GPVTG,122.75,T,,M,0.08,N,0.15,K,A*32

Nmea-parser will only parse lines for recommended minimum specific GPS data; lines starting with $GPRMC.

NPM Package: https://www.npmjs.com/package/nmea-parser